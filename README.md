## FISP
A compiler for the [risp](http://github.com/raoulvdberge/risp) programming language, written in Risp. It's a lisp 
language implemented in Go.

I'm currently attempting to write a compiler for the language in itself. The 
initial language may need a few features to actually make this plausible, i.e. command
line arguments, reading and writing to files, and a few extra utilities to make
things quicker and cleaner.

## Disclaimer
I'm not a lisp programmer, let alone a functional programmer, so this may or may
not be idiomatic lispy code? In other words... the code probably sucks.

Another disclaimer: the style of the code is really weird because my eye sight sucks
and Vim makes it hard to properly match the brackets so I've tabbed most of it like it's
"normal" Java-y/C/whatever code.

## What's left:

* [x] Lexer - mostly done, might need some tweaks later in it's life;
* [ ] Parser - in progress;
* [ ] Semantic; and
* [ ] Code Generation;

## License
MIT. See the [license](/LICENSE).
